#!/bin/bash
set -euo pipefail

echo '
    ____  ________  ____  ____ _________
   / __ \/ ___/ _ \/ __ \/ __ `/ ___/ _ \
  / /_/ / /  /  __/ /_/ / /_/ / /  /  __/
 / .___/_/   \___/ .___/\__,_/_/   \___/
/_/             /_/
'

# Allow running the tests locally
if [ ! -v CI_PROJECT_DIR ]; then
    export CI_PROJECT_DIR=$(pwd)
    export CI_PIPELINE_ID=12345
fi
# Add some example projects to test.
export GITLAB_COM_PACKAGES="cki-lib datadefinition skt kpet upt"
export GITLAB_COM_DATA_PROJECTS="kpet-db"
export GITLAB_CEE_PACKAGES=""
export cki_lib_pip_url="git+https://gitlab.com/cki-project/cki-lib.git@master"
export kpet_db_pip_url="git+https://gitlab.com/cki-project/kpet-db.git@master"
# Additional variables normally supplied via the pipeline.
export PIPELINE_DEFINITION_URL=https://gitlab.com/cki-project/pipeline-definition.git
export PIPELINE_DEFINITION_DIR=${CI_PROJECT_DIR}
export SOFTWARE_OBJECT_SUFFIX=python
export SOFTWARE_DIR=${CI_PROJECT_DIR}/software
export VENV_DIR=${SOFTWARE_DIR}/venv
export VENV_BIN=${VENV_DIR}/bin
export VENV_PY3=${VENV_BIN}/python3

SOFTWARE_OBJECT=pipeline-${CI_PIPELINE_ID}-${SOFTWARE_OBJECT_SUFFIX}.tar.xz

# Verify the prepare stage.
function verify_prepare {
  source scripts/functions.sh

  echo_green "Checking for object in minio."
  aws_s3_ls BUCKET_SOFTWARE "${SOFTWARE_OBJECT}"

  echo_green "Downloading object and extracting it."
  aws_s3_download BUCKET_SOFTWARE "${SOFTWARE_OBJECT}" .
  mkdir /tmp/software_extracted
  tar -xf ${SOFTWARE_OBJECT} -C /tmp/software_extracted

  pushd /tmp/software_extracted
    echo_green "Verifying directories are present."
    test -d software
    test -d software/pipeline-definition
    test -d software/venv

    echo_green "Verifying Python packages."
    for PROJECT in $GITLAB_COM_PACKAGES; do
        ${VENV_PY3} -m pip show "${PROJECT}"
    done
    for PROJECT in $GITLAB_COM_DATA_PROJECTS; do
        test -d "software/${PROJECT}"
    done
  popd

  rm -rf /tmp/software_extracted ${SOFTWARE_OBJECT}

  echo_green "🥳🤩 SUCCESS!"
}

eval "$(tests/setup-minio.sh)"

# Run the prepare stage
source scripts/prepare_software.sh
verify_prepare

# Run the prepare stage when the software is already present.
source scripts/prepare_software.sh
verify_prepare

# Run the prepare stage when the software is packaged in object storage
# but not present on the host.
rm -rf software
source scripts/prepare_software.sh
verify_prepare

# Run the prepare stage when the software is packaged in object storage
# and an update is forced
echo invalid-tar-file > /tmp/invalid.txt
tar -cf - /tmp/invalid.txt | xz -z - > /tmp/invalid.tar.xz
aws --endpoint-url "${LOCALMINIO_ENDPOINT}" s3 cp /tmp/invalid.tar.xz "s3://${SOFTWARE_BUCKET}/${SOFTWARE_PATH}${SOFTWARE_OBJECT}"
rm -rf software/wheels
FORCE_SOFTWARE_PREPARE=true
source scripts/prepare_software.sh
verify_prepare

# Cleanup
rm -rf wheel-verify-venv software
